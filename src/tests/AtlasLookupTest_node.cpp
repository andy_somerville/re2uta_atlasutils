/*
 * AtlasLookupTest_node.cpp
 *
 *  Created on: Mar 28, 2013
 *      Author: andrew.somerville
 */



#include <re2uta/AtlasLookup.hpp>

#include <robot_state_publisher/robot_state_publisher.h>

#include <tf/transform_broadcaster.h>

#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>

#include <ros/ros.h>

#include <Eigen/Core>

#include <iostream>
#include <fstream>


using namespace re2uta;

class AtlasLookupTest
{
    private:
        typedef std::map<std::string, double> JointPositionMap;

        ros::NodeHandle  m_node;
        KDL::Tree        m_tree;
        urdf::Model      m_urdfModel;
        Eigen::VectorXd  m_jointPositions;
        Eigen::VectorXd  m_jointMins;
        Eigen::VectorXd  m_jointMaxs;
        JointPositionMap m_jointPositionMap;

    public:
        AtlasLookupTest()
        {
            std::string simpleUrdfString;

            m_node.getParam( "/robot_description", simpleUrdfString );

            m_urdfModel.initString( simpleUrdfString );

            kdl_parser::treeFromString( simpleUrdfString, m_tree );
        }

        void go()
        {
            AtlasLookup atlasLookup( m_urdfModel );
            typedef AtlasLookup::StringToIndexMap StringToIndexMap;
            int i = 0;

            BOOST_FOREACH( const StringToIndexMap::value_type & element, atlasLookup.getJointNameToIndexMap()    )
            {
                ROS_INFO_STREAM(    "raw name: "     << element.first
                                 << "\tjoint index " << element.second
                                 << "\tqnr "         << atlasLookup.jointNameToQnr( element.first ) );
            }

            i = 0;
            BOOST_FOREACH( const std::string & element, atlasLookup.getJointNameVector() )
            {
                double min = atlasLookup.getCmdJointMinVector()(i);
                double max = atlasLookup.getCmdJointMaxVector()(i);
                ROS_INFO_STREAM( "joint index: " << i << "\tjoint name " << element << "\tmin: " << min << "\tmax: "<< max );
                ++i;
            }

            i = 0;
            BOOST_FOREACH( int qnr, atlasLookup.getJointToTreeQnrIndexVector() )
            {
                double min = atlasLookup.getTreeJointMinVector()(qnr);
                double max = atlasLookup.getTreeJointMaxVector()(qnr);
                ROS_INFO_STREAM(    "joint index: " << i
                                 << "\tqnr "        << qnr
                                 << "\tjoint name " << atlasLookup.jointIndexToJointName(i)
                                 << "\tmin: " << min
                                 << "\tmax: " << max );

                ++i;
            }

            for( unsigned int qnr = 0; qnr < atlasLookup.getTreeQnrToJointIndexVector().size(); ++qnr )
            {
                int resolvedQnr = atlasLookup.segmentNameToQnr( atlasLookup.treeQnrToSegmentName( qnr ) );
                ROS_INFO_STREAM(    "qnr: "          << qnr
                                 << "\tjoint index " << atlasLookup.treeQnrToJointIndex(qnr)
                                 << "\tjoint name "  << atlasLookup.treeQnrToJointName( qnr )
                                 << "\tsegment name " << atlasLookup.treeQnrToSegmentName( qnr )
                                 << " resolved qnr "<< resolvedQnr );
            }


            Eigen::VectorXd treeJointAngles(atlasLookup.getNumTreeJoints());
            for( int i = 0; i < treeJointAngles.size(); ++i )
                treeJointAngles( i ) = i;

            Eigen::VectorXd cmdJointAngles(atlasLookup.getNumCmdJoints());
            for( int i = 0; i < cmdJointAngles.size(); ++i )
                cmdJointAngles( i ) = i;

            Eigen::VectorXd treeToCmdJointAngles = atlasLookup.treeToCmd( treeJointAngles );
            Eigen::VectorXd cmdToTreeJointAngles = atlasLookup.cmdToTree( cmdJointAngles  );

            ROS_INFO_STREAM( "TreeToCmd result: \n" << treeToCmdJointAngles );
            ROS_INFO_STREAM( "CmdToTree result: \n" << cmdToTreeJointAngles );

            ROS_INFO_STREAM( "Default joint cmd msg: \n" << *atlasLookup.createDefaultJointCmdMsg( m_node ) );


        }
};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "atlas_debug_publisher_test" );
    ros::NodeHandle node;

    AtlasLookupTest atlasLookupTest;

    atlasLookupTest.go();

    return 0;
}
