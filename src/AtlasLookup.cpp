/*
 * AtlasLookup.cpp
 *
 *  Created on: Mar 28, 2013
 *      Author: andrew.somerville
 */

#include <atlas_msgs/AtlasCommand.h>
#include <atlas_msgs/AtlasState.h>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <urdf/model.h>
#include <boost/foreach.hpp>

namespace
{
    double nonNaN( const double & value )
    {
        if( std::isinf(value) || std::isnan(value) )
            return 0;
        else
            return value;
    }
}


namespace re2uta
{


const std::string AtlasLookup::emptyString;

AtlasLookup::
AtlasLookup( const urdf::Model & model )
{
    m_tree.reset( new KDL::Tree );
    kdlRootInertiaWorkaround( model, *m_tree );
    re2::kdltools::getJointLimits( model, m_jointMinMap, m_jointMaxMap );

    //FIXME this is done differently from the rest which are generated when needed
    ros::NodeHandle node;
    node.getParam( "/drc/atlas_translations", m_translationMap );
}


boost::shared_ptr<const KDL::Tree>
AtlasLookup::
getTree()
{
    return m_tree;
}

int
AtlasLookup::
getNumTreeJoints()
{
    return m_tree->getNrOfJoints();
}

int
AtlasLookup::
getNumCmdJoints()
{
    return getJointNameVector().size();
}


atlas_msgs::AtlasCommand::Ptr
AtlasLookup::
createEmptyJointCmdMsg()
{
    atlas_msgs::AtlasCommand::Ptr msg( new atlas_msgs::AtlasCommand );

    msg->kp_position .resize( getNumCmdJoints(), 0 );
    msg->kd_position .resize( getNumCmdJoints(), 0 );
    msg->kp_velocity .resize( getNumCmdJoints(), 0 );
    msg->ki_position .resize( getNumCmdJoints(), 0 );
    msg->position    .resize( getNumCmdJoints(), 0 );
    msg->velocity    .resize( getNumCmdJoints(), 0 );
    msg->effort      .resize( getNumCmdJoints(), 0 );
    msg->i_effort_max.resize( getNumCmdJoints(), 0 );
    msg->i_effort_min.resize( getNumCmdJoints(), 0 );
    msg->k_effort    .resize( getNumCmdJoints(), 0 );

    return msg;
}



atlas_msgs::AtlasCommand::Ptr
AtlasLookup::
createDefaultJointCmdMsg( ros::NodeHandle node )
{
    atlas_msgs::AtlasCommand::Ptr msg;

    msg = createEmptyJointCmdMsg();

    XmlRpc::XmlRpcValue gainParams;
    bool haveGains;
    haveGains = node.getParam( "/atlas_controller/gains", gainParams );

    if( gainParams.begin() == gainParams.end() )
    {
        ROS_ERROR( "AtlasLookup::createDefaultJointCmdMsg: NO GAINS READ!" );
    }

    XmlRpc::XmlRpcValue::iterator jointItor;
    for( jointItor = gainParams.begin(); jointItor != gainParams.end(); ++ jointItor )
    {
        XmlRpc::XmlRpcValue & jointGains = jointItor->second;
        std::string jointName = jointItor->first;

        int jointIndex = jointNameToJointIndex( jointName );
        ROS_DEBUG_STREAM( "Joint name: " << jointName );
        ROS_DEBUG_STREAM( "Joint index: " << jointIndex );

        if( jointIndex >= 0 )
        {
            XmlRpc::XmlRpcValue::iterator jointGainsItor;
            for( jointGainsItor = jointGains.begin(); jointGainsItor != jointGains.end(); ++ jointGainsItor )
            {
                XmlRpc::XmlRpcValue & gain = jointGainsItor->second;
                ROS_DEBUG_STREAM( "gain name: " << jointGainsItor->first );
                ROS_DEBUG_STREAM( "gain value: " << (double&)gain );

                if( jointGainsItor->first == "p" )
                    msg->kp_position[jointIndex]  =  (double&)gain;
                if( jointGainsItor->first == "d" )
                    msg->kd_position[jointIndex]  =  (double&)gain;
                if( jointGainsItor->first == "i" )
                    msg->ki_position[jointIndex]  =  (double&)gain;
                if( jointGainsItor->first == "i_clamp" )
                    msg->i_effort_min[jointIndex] = -(double&)gain;
                if( jointGainsItor->first == "i_clamp" )
                    msg->i_effort_max[jointIndex] =  (double&)gain;

            }
        }
    }


    return msg;
}





const std::vector<std::string> &
AtlasLookup::
getJointNameVector()
{
    if( m_jointIndexToNameVector.empty() )
    {
    	ros::NodeHandle node;
    	if( ! node.getParam("/drc/standard_joint_order", m_jointIndexToNameVector ) )
    	    if( ! node.getParam("standard_joint_order", m_jointIndexToNameVector ) )
    	        ROS_ASSERT_MSG( false, "missing param for standard_joint_order" );
    }

    return m_jointIndexToNameVector;
}

Eigen::VectorXd
AtlasLookup::
calcDefaultTreeJointAngles( double legBend )
{
    Eigen::VectorXd defaultTreeJointAngles = Eigen::VectorXd::Zero( getNumTreeJoints() );

    defaultTreeJointAngles = ((getTreeJointMaxVector() + getTreeJointMinVector())/2).unaryExpr( std::ptr_fun( nonNaN ) );

    ros::NodeHandle node;
    typedef std::map<std::string, double> StringDoubleMap;
    StringDoubleMap jointPosMap;

    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("l_lleg" ) ) ] =  legBend * 2;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("r_lleg" ) ) ] =  legBend * 2;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("l_talus") ) ] = -legBend;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("r_talus") ) ] = -legBend;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("l_uleg" ) ) ] = -legBend - 0.02;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("r_uleg" ) ) ] = -legBend - 0.02;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("l_uglut") ) ] =  0;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("r_uglut") ) ] =  0;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("l_lglut") ) ] =  0;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("r_lglut") ) ] =  0;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("r_scap" ) ) ] =  1.3;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("l_scap" ) ) ] = -1.3;
    jointPosMap[ jointIndexToJointName( segmentNameToJointIndex("head"   ) ) ] =  0.6;

    if( !node.getParam("/drc/pid_stand", jointPosMap ) ) // overwrite defaults where they overlap
        ROS_WARN( "Did not find /drc/pid_stand. using defaults" );
    else
        ROS_WARN( "Found /drc/pid_stand. using param values" );


    BOOST_FOREACH( StringDoubleMap::value_type & entry, jointPosMap )
    {
        std::string const & jointName = entry.first;
        double const &      value     = entry.second;

        int qNum = jointNameToQnr( jointName );

        if( qNum > -1 ) // ignore/reject bad lookup
        {
            defaultTreeJointAngles( qNum ) = value;
            ROS_WARN( "setting qn: %i, %s to %f", qNum, jointName.c_str(), value );
        }
        else
            ROS_WARN( "ignoring bad value: %s", jointName.c_str() );

    }

    return defaultTreeJointAngles;
}


Eigen::VectorXd
AtlasLookup::
calcDefaultCmdJointAngles( double legBend )
{
    return treeToCmd( calcDefaultTreeJointAngles( legBend ) );
}



std::string
AtlasLookup::
lookupString( std::string const & query )
{
    std::string value = query;

    std::map<std::string,std::string>::iterator mapResult;
    mapResult = m_translationMap.find( query );
    if( mapResult != m_translationMap.end() )
        value = mapResult->second;
//    else
//        ROS_WARN( "No lookup for: %s, pass through", value.c_str() );

    return value;
}



const std::vector<std::string> &
AtlasLookup::
getJointPrefixedNameVector()
{
    if( m_jointIndexToPrefixedNameVector.empty() )
    {
        m_jointIndexToPrefixedNameVector.reserve(getJointNameVector().size());
        BOOST_FOREACH( const std::string & jointName, getJointNameVector() )
        {
            //FIXME AJS "atlas::" should be not be hard coded, but current implications unclear
            m_jointIndexToPrefixedNameVector.push_back( std::string( "atlas::") + jointName );
        }
    }
    return m_jointIndexToPrefixedNameVector;
}


const AtlasLookup::StringToIndexMap &
AtlasLookup::
getJointNameToIndexMap()
{
    if( m_jointNameToIndexMap.empty() )
    {
        int index = 0;
        BOOST_FOREACH( const std::string & name, getJointNameVector() )
        {
            m_jointNameToIndexMap[name] = index;
            ++index;
        }
    }
    return m_jointNameToIndexMap;
}


const AtlasLookup::StringToIndexMap &
AtlasLookup::
getJointPrefixedNameToIndexMap( const std::vector<std::string> & nameVector )
{
    if( m_jointPrefixedNameToIndexMap.empty() )
    {
        int index = 0;
        BOOST_FOREACH( const std::string & name, getJointPrefixedNameVector() )
        {
            m_jointPrefixedNameToIndexMap[name] = index;
            ++index;
        }
    }
    return m_jointPrefixedNameToIndexMap;
}


const Eigen::VectorXd &
AtlasLookup::
getTreeJointMinVector()
{
    if( m_treeJointMins.size() == 0 )
        buildKdlTreeBasedMaps();

    return m_treeJointMins;
}

const Eigen::VectorXd &
AtlasLookup::
getTreeJointMaxVector()
{
    if( m_treeJointMaxs.size() == 0  )
        buildKdlTreeBasedMaps();

    return m_treeJointMaxs;
}

const Eigen::VectorXd &
AtlasLookup::
getCmdJointMinVector()
{
    if( m_cmdJointMins.size() == 0  )
        buildKdlTreeBasedMaps();

    return m_cmdJointMins;
}

const Eigen::VectorXd &
AtlasLookup::
getCmdJointMaxVector()
{
    if( m_cmdJointMaxs.size() == 0  )
        buildKdlTreeBasedMaps();

    return m_cmdJointMaxs;
}




const KDL::TreeElement *
AtlasLookup::
lookupTreeElementFromJointName( const std::string & jointName )
{
    if( m_jointNameToTreeElementMap.empty() )
    {
        buildKdlTreeBasedMaps();
        assert( ! m_jointNameToTreeElementMap.empty() );
    }

    JointNameToTreeElementMap::iterator entry;
    entry = m_jointNameToTreeElementMap.find( jointName );
    if( entry != m_jointNameToTreeElementMap.end() )
        return entry->second;
    else
        return NULL;
}


int
AtlasLookup::
treeQnrToJointIndex( int qnr )
{
    const std::vector<int> & qnrToJointIndex = getTreeQnrToJointIndexVector();

    if( qnr < qnrToJointIndex.size() )
        return qnrToJointIndex[qnr];
    else
        return -1;
}


int
AtlasLookup::
jointIndexToQnr( int jointIndex )
{
    const std::vector<int> & jointIndexToQnr = getJointToTreeQnrIndexVector();

    if( jointIndex >= 0 && jointIndex < jointIndexToQnr.size() )
        return jointIndexToQnr[ jointIndex ];
    else
    {
        ROS_ASSERT_MSG( false, "invalid joint index in jointIndexToQnr: %i", jointIndex );
        ROS_ERROR_STREAM(      "invalid joint index in jointIndexToQnr: " << jointIndex );
        return -1;
    }
}

const std::string &
AtlasLookup::
jointIndexToJointName( int jointIndex )
{
    const std::vector<std::string> & jointIndexToName = getJointNameVector();

    if( jointIndex >= 0 && jointIndex < jointIndexToName.size() )
        return jointIndexToName[ jointIndex ];
    else
    {
//        ROS_ASSERT_MSG( false, "invalid joint index in jointIndexToJointName: %i", jointIndex );
        ROS_ERROR_STREAM(      "invalid joint index in jointIndexToJointName: " << jointIndex );
        return emptyString;
    }
}

const std::string &
AtlasLookup::
jointIndexToSegmentName( int jointIndex )
{
    return treeQnrToSegmentName( jointIndexToQnr( jointIndex ) );
}


int
AtlasLookup::
jointNameToQnr( const std::string & jointName )
{
    const KDL::TreeElement * treeElement = lookupTreeElementFromJointName( jointName );

    if( treeElement != NULL )
        return treeElement->q_nr;
    else
    {
        ROS_ERROR_STREAM(      "invalid joint name in jointNameToQnr: " << jointName );
        return -1;
    }
}

int
AtlasLookup::
segmentNameToQnr( const std::string & segmentName )
{
    KDL::SegmentMap::const_iterator element = m_tree->getSegments().find( segmentName );

    if( element != m_tree->getSegments().end() )
    {
        if( element->second.segment.getJoint().getType() == KDL::Joint::None )
            ROS_WARN_STREAM( "Joint for segment: " << segmentName << " is fixed, qnr "<< element->second.q_nr << " may be wrong" );
        return element->second.q_nr;
    }
    else
    {
        ROS_ERROR_STREAM(      "invalid segment name in segmentNameToQnr: " << segmentName );
        return -1;
    }
}

int
AtlasLookup::
segmentNameToJointIndex( const std::string & segmentName )
{
    return treeQnrToJointIndex( segmentNameToQnr(segmentName) );
}



const std::string &
AtlasLookup::
treeQnrToSegmentName( int qnr )
{
    const std::vector<const KDL::TreeElement*> & qnrToTreeElementVector = getQnrToTreeElementVector();

    if( qnr >= 0 && qnr < qnrToTreeElementVector.size() )
        return qnrToTreeElementVector[qnr]->segment.getName();
    else
        return emptyString;
}


const std::string &
AtlasLookup::
treeQnrToJointName( int qnr )
{
    const std::vector<const KDL::TreeElement*> & qnrToTreeElementVector = getQnrToTreeElementVector();

    if( qnr >= 0 && qnr < qnrToTreeElementVector.size() )
        return qnrToTreeElementVector[qnr]->segment.getJoint().getName();
    else
        return emptyString;
}



const std::vector<int> &
AtlasLookup::
getTreeQnrToJointIndexVector()
{
    if( m_treeQnrToJointIndexVector.empty() )
    {
        buildKdlTreeBasedMaps();

        assert( !m_treeQnrToJointIndexVector.empty() );
    }

    return m_treeQnrToJointIndexVector;
}


const std::vector<int> &
AtlasLookup::
getJointToTreeQnrIndexVector()
{
    if( m_treeJointIndexToQnrVector.empty() )
    {
        buildKdlTreeBasedMaps();

        assert( !m_treeJointIndexToQnrVector.empty() );
    }

    return m_treeJointIndexToQnrVector;
}

const std::vector<const KDL::TreeElement*> &
AtlasLookup::
getQnrToTreeElementVector()
{
    if( m_qnrToTreeElementVector.empty() )
    {
        buildKdlTreeBasedMaps();

        assert( !m_qnrToTreeElementVector.empty() );
    }

    return m_qnrToTreeElementVector;
}


void
AtlasLookup::
buildKdlTreeBasedMaps()
{
    m_treeQnrToJointIndexVector.resize( m_tree->getNrOfJoints(),     -1   );
    m_treeJointIndexToQnrVector.resize( getJointNameVector().size(), -1   );
    m_qnrToTreeElementVector.resize(    m_tree->getNrOfJoints(),     NULL );

    m_cmdJointMins  = Eigen::VectorXd::Ones( getJointNameVector().size() ) * std::numeric_limits<double>::quiet_NaN();
    m_cmdJointMaxs  = Eigen::VectorXd::Ones( getJointNameVector().size() ) * std::numeric_limits<double>::quiet_NaN();
    m_treeJointMins = Eigen::VectorXd::Ones( m_tree->getNrOfJoints()      ) * std::numeric_limits<double>::quiet_NaN();
    m_treeJointMaxs = Eigen::VectorXd::Ones( m_tree->getNrOfJoints()      ) * std::numeric_limits<double>::quiet_NaN();

    BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree->getSegments() )
    {
        const KDL::TreeElement & element    = entry.second;
        const KDL::Segment  &    segment    = element.segment;
        const KDL::Joint &       joint      = segment.getJoint();
        int                      qnr        = element.q_nr;
        int                      jointIndex = jointNameToJointIndex( joint.getName() );

        ROS_ASSERT( qnr < m_tree->getNrOfJoints() );

        m_jointNameToTreeElementMap[joint.getName()] = &element;
        ROS_INFO( "Adding joint [%s] with qnr [%i] to jointNameToTreeElementMap", joint.getName().c_str(), qnr );

        if( joint.getType() != KDL::Joint::None )
        {
            if(        qnr >= 0 &&        qnr < m_treeQnrToJointIndexVector.size() )
            {
                m_treeQnrToJointIndexVector[qnr]        = jointIndex; // joint index will sometimes be -1 and that's ok
                m_qnrToTreeElementVector[qnr]           = &element;
                m_treeJointMins(qnr)                    = m_jointMinMap[joint.getName()];
                m_treeJointMaxs(qnr)                    = m_jointMaxMap[joint.getName()];
            }

            if( jointIndex >= 0 && jointIndex < m_treeJointIndexToQnrVector.size() )
            {
                m_treeJointIndexToQnrVector[jointIndex] = qnr;
                m_cmdJointMins(jointIndex)              = m_jointMinMap[joint.getName()];
                m_cmdJointMaxs(jointIndex)              = m_jointMaxMap[joint.getName()];
            }
        }
    }
}


int
AtlasLookup::
jointNameToJointIndex( const std::string & rawName )
{
    StringToIndexMap::const_iterator entry;
    entry = getJointNameToIndexMap().find( rawName );

    if( entry != getJointNameToIndexMap().end() )
        return entry->second;
    else
        return -1;
}


Eigen::VectorXd
AtlasLookup::
treeToCmd( const Eigen::VectorXd & treeJointValues )
{
    ROS_ASSERT_MSG( treeJointValues.size() == getNumTreeJoints(), "treeJointValues is not the right size" );
    Eigen::VectorXd cmdJointValues = Eigen::VectorXd::Zero( getNumCmdJoints() );
    for( int i = 0; i < getNumCmdJoints(); ++i )
    {
        int qnr = jointIndexToQnr(i);
        if( qnr >= 0 )
            cmdJointValues( i ) = treeJointValues( qnr );
    }

    return cmdJointValues;
}

Eigen::VectorXd
AtlasLookup::
cmdToTree( const Eigen::VectorXd & cmdJointValues )
{
    ROS_ASSERT_MSG( cmdJointValues.size() == getNumCmdJoints(),
                    "cmdJointValues is not the right size. Should be: %i but is %i",  getNumCmdJoints(), cmdJointValues.size());
    Eigen::VectorXd treeJointValues = Eigen::VectorXd::Zero( getNumTreeJoints() );

    for( int qnr = 0; qnr < getNumTreeJoints(); ++qnr )
    {
        int jnt = treeQnrToJointIndex(qnr);
        if( jnt >= 0 )
            treeJointValues( qnr ) = cmdJointValues( jnt );
    }

    return treeJointValues;
}





}

